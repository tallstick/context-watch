import React, { Component } from 'react';
import './App.css';
import { Shake } from 'reshake';
import ReactCountdownClock from 'react-countdown-clock';

const schedule = [
  {"notif-type":"SHORT_BUZZ","next-activity":"LUNCH_APPROACHING","name":"IN_CLASS"},
  {"notif-type":"LONG_BUZZ","next-activity":"LUNCH_SOON","name":"LUNCH_APPROACHING"},
  {"notif-type":"VISUAL_COUNTDOWN","next-activity":"COUNTDOWN","name":"LUNCH_SOON"},
  {"notif-type":"BEEP","next-activity":"GO_TO_LUNCH","name":"COUNTDOWN"},
  {"notif-type":"NONE","next-activity":"NONE","name":"GO_TO_LUNCH"}];

class App extends Component {

  constructor(props) {
    super(props);
    this.audio = new Audio('/beep.mp3');
    this.state = {currentStep: {"notif-type":"SHORT_BUZZ","next-activity":"LUNCH_APPROACHING","name":"IN_CLASS"}, vibrate: 0};
  }

  componentWillMount() {
    this.fetchStep();
  }

  componentDidMount() {
    setInterval(() => {
      this.fetchStep();
    }, 1500)
  }

  shouldComponentUpdate(nextProps, nextState) {
    let ret = !(this.state.currentStep["notif-type"] === 'VISUAL_COUNTDOWN' && nextState.currentStep["notif-type"] === 'VISUAL_COUNTDOWN') && 
    !(this.state.currentStep["notif-type"] === 'SHORT_BUZZ' && this.state.vibrate === nextState.vibrate);
    //return ret;
    return nextState.vibrate !== this.state.vibrate || nextState.currentStep["notif-type"] !== this.state.currentStep["notif-type"];
  }

  fetchStep = () => {
    fetch('https://young-bayou-55448.herokuapp.com/step').then(function(response) {
      console.log(response);
      return response.json();
    }).then((data) => {
      console.log(data);
      let nextStep = data;
      if (nextStep.name !== this.state.currentStep.name) {
        this.notify(nextStep);
      }

      this.setState({currentStep: nextStep});
    });
  }

  notify = (nextStep) => {
    switch(nextStep["notif-type"]) {
      case 'SHORT_BUZZ':
        this.buzz(300);
        break;
      case 'LONG_BUZZ':
        this.buzz(1500);
        break;
      case 'VISUAL_COUNTDOWN':
        break;
      case 'BEEP':
        this.audio.play();
        break;
      case 'NONE':
        break;
    }
  }

  buzz = (length) => {
    console.log('vibrate for: ' + length);
    this.setState({vibrate: 2});
    setTimeout(() => {
      console.log('stop vibration');
      this.setState({vibrate: 0});
    }, length);
  }

  render() {
    let content;
    switch(this.state.currentStep["next-activity"]) {
      case 'LUNCH_APPROACHING':
      content = (
        <div className='centered'>
          <span className='centered' style={{color: 'black', fontSize: 20}}>Lunch approaching</span>
          <svg class = "button" expanded = "true" height = "400px" width = "400px">
          <circle class = "innerCircle" cx = "50%" cy = "50%" r = "25%" fill = "none" stroke = "#4d5986" stroke-width = "5%">
              <animate attributeType="SVG" attributeName="r" begin="0s" dur="2s" repeatCount="indefinite" from="5%" to="25%"/>
              <animate attributeType="CSS" attributeName="stroke-width" begin="0s"  dur="2s" repeatCount="indefinite" from="3%" to="0%" />
              <animate attributeType="CSS" attributeName="opacity" begin="0s"  dur="2s" repeatCount="indefinite" from="1" to="0"/>
            </circle>
        </svg>
        </div>
      );
        break;
      case 'LUNCH_SOON':
      content = (
        <div className='centered'>
                    <span className='centered' style={{color: 'black', fontSize: 20}}>Lunch soon</span>
          <svg class = "button" expanded = "true" height = "400px" width = "400px">
          <circle class = "innerCircle" cx = "50%" cy = "50%" r = "25%" fill = "none" stroke = "#4d5986" stroke-width = "5%">
              <animate attributeType="SVG" attributeName="r" begin="0s" dur="2s" repeatCount="indefinite" from="5%" to="25%"/>
              <animate attributeType="CSS" attributeName="stroke-width" begin="0s"  dur="2s" repeatCount="indefinite" from="3%" to="0%" />
              <animate attributeType="CSS" attributeName="opacity" begin="0s"  dur="2s" repeatCount="indefinite" from="1" to="0"/>
            </circle>
        </svg>
        </div>
      );
        break;
      case 'COUNTDOWN':
      content = (
        <div className='centered'>
<div class="timer">
    <div class="mask"></div>
</div>
        </div>
      );
        break;
      case 'GO_TO_LUNCH':
      content = (
        <div className='centered'>
                    <span className='centered' style={{color: 'black', fontSize: 20}}>Go to lunch</span>
          <svg class = "button" expanded = "true" height = "400px" width = "400px">
            <circle class = "innerCircle" cx = "50%" cy = "50%" r = "25%" fill = "none" stroke = "#4d5986" stroke-width = "5%">
              <animate attributeType="SVG" attributeName="r" begin="0s" dur="2s" repeatCount="indefinite" from="5%" to="25%"/>
              <animate attributeType="CSS" attributeName="stroke-width" begin="0s"  dur="2s" repeatCount="indefinite" from="3%" to="0%" />
              <animate attributeType="CSS" attributeName="opacity" begin="0s"  dur="2s" repeatCount="indefinite" from="1" to="0"/>
            </circle>
        </svg>
        </div>
      ); 
        break;
      case 'NONE':
      content = (
        <div className='centered'>
          <img style={{width: 260, height: 310, borderRadius: '15%'}} src={'http://www.ststephensglasgow.co.uk/wp-content/uploads/2014/10/Lunch.png'}/>
        </div>
      );
        break;
    }
    return (
      <div className="container ">
      <Shake
      fixed={true}
        h={this.state.vibrate} 
        v={this.state.vibrate} 
        r={this.state.vibrate}>        
        <div className="App">
          {content}
          <img src='/img/watch.jpg'/>
        </div>
      </Shake>
      </div>
    );
  }
}

export default App;
